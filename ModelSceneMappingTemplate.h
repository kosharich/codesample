#ifndef INCLUDE_GUARD_Kosharich_20151215_123414_
#define INCLUDE_GUARD_Kosharich_20151215_123414_


#include "sea3d/CorePrerequisites.h"
#include "sea3d/RefCounted.h"
#include "sea3d/Assertions.h"

#include "sea3d/gfx/ISceneGraph.h"

#include <map>

namespace sea3d
{
    SEA3D_DECLARE_REFCOUNTED(IModel);
    SEA3D_DECLARE_REFCOUNTED(ModelObject);

    namespace gfx
    {
        class ISceneGraph;
        SEA3D_DECLARE_REFCOUNTED(ISceneGraph);
    }

}

namespace sea3d
{
    namespace detail
    {
        template<typename ModelObjectT, typename SceneObjectT>
        void clearScene(gfx::ISceneGraph* scene, std::map<SceneObjectT, ModelObjectT>* objects)
        {
            assert(scene != nullptr);
            assert(objects!= nullptr);

            typedef std::map<SceneObjectT, ModelObjectT> SceneObjects;

            for (SceneObjects::const_iterator it = objects->begin(), end = objects->end(); it != end; ++it)
                scene->destroyGeometry(it->first);
        }

        template<typename ModelObjectT, typename SceneObjectT>
        class AtExitSceneClearer
        {
        public:
            typedef std::map<SceneObjectT, ModelObjectT> SceneObjects;

        public:
            AtExitSceneClearer(gfx::ISceneGraph* scene, SceneObjects* objects)
                : _scene(scene)
                , _objects(objects)
            {
                assert(_scene != nullptr);
                assert(_geometries != nullptr);
            }

            ~AtExitSceneClearer()
            {
                if (_scene == nullptr)
                    return;

                detail::clearScene<ModelObjectT, SceneObjectT>(_scene, _objects);
            }

            void reset()
            {
                _scene = nullptr;
                _geometries = nullptr;
            }

        private:
            gfx::ISceneGraph* _scene;
            SceneObjects* _objects;
        };
    }


    template<typename ModelObjectT, typename SceneObjectT>
    class ModelSceneMappingTemplate : virtual public RefCounted
    {
    public:
        typedef std::vector<ModelObjectT> ModelObjects;
        typedef std::vector<SceneObjectT> SceneObjects;

    public:
        ModelSceneMappingTemplate() {}
        ~ModelSceneMappingTemplate()
        {
            if (!isReleased())
                release();
        }

        ModelSceneMappingTemplate( IModelPtr model, gfx::ISceneGraphPtr scene )
            : _model(model)
            , _scene(scene)
        {
            SEA3D_CHECK_ARGUMENT(model);
            SEA3D_CHECK_ARGUMENT(scene);
        }

        IModelPtr model() const { return _model; }
        gfx::ISceneGraphPtr scene() const { return _scene; }


        ModelObjects modelObjects(const string& name) const { return findObjects(name, _modelObjectByName); }
        ModelObjectT modelObject(SceneObjectT sceneObject) const
        {
            return findObject(sceneObject, _modelObjectBySceneObject);
        }

        SceneObjects sceneObjects(const string& name) const { return findObjects(name, _sceneObjectByName); }
        SceneObjectT sceneObject(ModelObjectT modelObject) const
        {
            return findObject(modelObject, _sceneObjectByModelObject);
        }

        bool isReleased() const { return _scene == nullptr && _model == nullptr; }

        void release()
        {
            detail::clearScene<ModelObjectT, SceneObjectT>(_scene.get(), &_modelObjectBySceneObject);

            _modelObjectBySceneObject.clear();
            _sceneObjectByModelObject.clear();
            _modelObjectByName.clear();
            _sceneObjectByName.clear();

            _scene = nullptr;
            _model = nullptr;
        }

    protected:
        typedef std::multimap<string, ModelObjectT> ModelObjectByName;
        typedef std::map<SceneObjectT, ModelObjectT> ModelObjectBySceneObject;


        typedef std::multimap<string, SceneObjectT> SceneObjectByName;
        typedef std::map<ModelObjectT, SceneObjectT> SceneObjectByModelObject;

    protected:
        void addNewObject(ModelObjectT modelObject, SceneObjectT sceneObject)
        {
            _modelObjectByName.insert(std::make_pair(modelObject->name(), modelObject));
            _modelObjectBySceneObject.insert(std::make_pair(sceneObject, modelObject));

            _sceneObjectByName.insert(std::make_pair(modelObject->name(), sceneObject));
            _sceneObjectByModelObject.insert(std::make_pair(modelObject, sceneObject));
        }

        const ModelObjectBySceneObject& forwardMapping() const { return _modelObjectBySceneObject; }
        const SceneObjectByModelObject& backwardMapping() const { return _sceneObjectByModelObject; }

    private:
        template<typename ObjectT, typename IndexObjectT>
        ObjectT findObject(IndexObjectT indexObject, const std::map<IndexObjectT, ObjectT>& index) const
        {
            typedef std::map<IndexObjectT, ObjectT> Index;
            typename Index::const_iterator it = index.find(indexObject);

            return it != index.end() ? it->second : ObjectT();
        }

        template<typename ObjectT>
        std::vector<ObjectT> findObjects(const string& name, const std::multimap<string, ObjectT>& index) const
        {
            typedef std::vector<ObjectT> Objects;
            typedef std::multimap<string, ObjectT> Index;
            typedef typename Index::const_iterator IndexIterator;
            typedef std::pair<IndexIterator, IndexIterator> ContentRange;

            const ContentRange range = index.equal_range(name);

            Objects objects;

            for (IndexIterator it = range.first, end = range.second; it != end; ++it)
                objects.push_back(it->second);

            return objects;
        }

    private:
        IModelPtr _model;
        gfx::ISceneGraphPtr _scene;

        ModelObjectByName _modelObjectByName;
        SceneObjectByName _sceneObjectByName;
        ModelObjectBySceneObject _modelObjectBySceneObject;
        SceneObjectByModelObject _sceneObjectByModelObject;
    };

}

#endif
