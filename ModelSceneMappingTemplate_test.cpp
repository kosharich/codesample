#include "sea3d/ModelSceneMappingTemplate.h"
#include "sea3d/ModelObjectIterator.h"
#include "sea3d/utils/StringHelpers.h"

#include "sea3d/Model.h"

#include "sea3d/IModel_test.h"
#include "sea3d/gfx/ISceneGraph_test.h"
#include "sea3d/gfx/IGeometry_test.h"

#include "sea3d/Testing.h"

#include <boost/function.hpp>

namespace sea3d
{
    using ::testing::ContainerEq;

    using gfx::tests::SceneGraphMock;
    using gfx::tests::SceneGraphStub;
    using gfx::tests::SceneGraphStubPtr;
    using gfx::tests::SceneGraphMockPtr;

namespace tests
{
    using ::testing::_;

    class ModelObjectStub
    {
    public:
        ModelObjectStub(const string& name)
            : _name(name)
        {
        }

        string name() const { return _name; }

    private:
        string _name;
    };

    class SceneObjectStub : public gfx::IGeometry
    {
    public:
        SceneObjectStub() {}

        virtual void setPosition(const Vector& position) { setPosition(position.x, position.y, position.z); }
        virtual void setPosition(float x, float y, float z) {}
        virtual Vector position() const { return Vector(); }

        virtual void setOrientation(const Quaternion& orientation) {}
        virtual Quaternion orientation() const { return Quaternion::identity(); }

        virtual void setScale(const Vector& scale) {}
        virtual void setScale(float scale) {}

        virtual void setHidingDistance(float distance) {}
        virtual float hidingDistance() const { return 0.0f; }

        virtual Box boundingBox() const { return Box(); }
    };

    // ----------------------------------------------------------------------------------------

    typedef std::vector<ModelObjectStub*> ModelObjectStubPtrs;
    typedef std::vector<SceneObjectStub*> SceneObjectStubPtrs;

    typedef std::vector<ModelObjectStub> ModelObjectStubs;
    typedef std::vector<SceneObjectStub> SceneObjectStubs;

    typedef std::vector<string> ObjectNames;

    // ----------------------------------------------------------------------------------------

    SEA3D_DECLARE_REFCOUNTED(TestModelSceneMapping);
    class TestModelSceneMapping : public ModelSceneMappingTemplate<ModelObjectStub*, SceneObjectStub*>
    {
    public:
        TestModelSceneMapping() {}
        ~TestModelSceneMapping() {}

        TestModelSceneMapping( IModelPtr model, gfx::ISceneGraphPtr scene )
            : ModelSceneMappingTemplate<ModelObjectStub*, SceneObjectStub*>(model, scene)
        {}

        void fillData(const ModelObjectStubPtrs& modelObjects, const SceneObjectStubPtrs& sceneObjects)
        {
            assert(modelObjects.size() == sceneObjects.size());

            const int size = modelObjects.size();

            for (int i = 0; i < size; ++i)
            {
                addNewObject(modelObjects.at(i), sceneObjects.at(i));
            }
        }
    };

    // ----------------------------------------------------------------------------------------

    TEST(ModelSceneMappingTemplateConstructionTest, nonValidSceneInConstructorThrowsError)
    {
        EXPECT_THROW({
            makeRefCounted<TestModelSceneMapping>(makeRefCounted<Model>(), gfx::ISceneGraphPtr(nullptr));
        }, ArgumentError);
    }

    TEST(ModelSceneMappingTempalateConstructionTest, nonValidModelInConstructorThrowsError)
    {
        EXPECT_THROW({
            makeRefCounted<TestModelSceneMapping>(IModelPtr(nullptr), makeRefCounted<SceneGraphMock>());
        }, ArgumentError);
    }

    TEST(ModelSceneMappingTempalateConstructionTest, isReleasedShouldReturnFalseBeforeReleaseCallAndTrueAfter)
    {
        TestModelSceneMappingPtr mapper =
            makeRefCounted<TestModelSceneMapping>(makeRefCounted<Model>(),  makeRefCounted<SceneGraphMock>());
        EXPECT_FALSE(mapper->isReleased());
        mapper->release();
        EXPECT_TRUE(mapper->isReleased());
    }

    // ----------------------------------------------------------------------------------------

    struct ModelSceneMappingTemplateTest : public ::testing::Test
    {
        TestModelSceneMappingPtr mapping;

        ModelObjectStubs modelObjects;
        SceneObjectStubs sceneObjects;

        ModelObjectStubPtrs modelObjectPtrs;
        SceneObjectStubPtrs sceneObjectPtrs;

        boost::function<TestModelSceneMapping::ModelObjects(const string&)> getModelObjects;
        boost::function<ModelObjectStub*(SceneObjectStub*)> getModelObject;

        boost::function<TestModelSceneMapping::SceneObjects(const string&)> getSceneObjects;
        boost::function<SceneObjectStub*(ModelObjectStub*)> getSceneObject;

        SceneGraphStubPtr scene;

        ModelSceneMappingTemplateTest()
        {
            scene = makeRefCounted<SceneGraphStub>();

            mapping = makeRefCounted<TestModelSceneMapping>(makeRefCounted<Model>(), scene);

            getModelObjects = boost::bind(&TestModelSceneMapping::modelObjects, mapping.get(), _1);
            getModelObject = boost::bind(&TestModelSceneMapping::modelObject, mapping.get(), _1);

            getSceneObjects = boost::bind(&TestModelSceneMapping::sceneObjects, mapping.get(), _1);
            getSceneObject = boost::bind(&TestModelSceneMapping::sceneObject, mapping.get(), _1);
        }

        ObjectNames createObjectNames(const char* names[], size_t size)
        {
            assert(names);

            return ObjectNames(names, names + size);
        }

        void fillData(const ObjectNames& names)
        {
            modelObjects.reserve(names.size());
            sceneObjects.reserve(names.size());

            for (ObjectNames::const_iterator it = names.begin(), end = names.end(); it != end; ++it)
            {
                modelObjects.push_back(ModelObjectStub(*it));
                modelObjectPtrs.push_back(&modelObjects.back());

                sceneObjects.push_back(SceneObjectStub());
                sceneObjectPtrs.push_back(&sceneObjects.back());
            }

            mapping->fillData(modelObjectPtrs, sceneObjectPtrs);
        }

        template<typename ObjectT, typename ObjectsT, typename SourceObjectsT>
        void testGetObjectsByName(
            boost::function<ObjectsT(const string&)> getObjectsFunction, bool existName,
            const SourceObjectsT& sourceData)
        {
            const char* tmp[] = {"test", "test", "test", "x", "y"};
            ObjectNames names = createObjectNames(tmp, 5);

            fillData(names);

            ObjectsT expectedObjects;
            for (int i = 0; i < 3; ++i)
                expectedObjects.push_back(sourceData[i]);

            if (existName)
                EXPECT_THAT(getObjectsFunction("test"), ContainerEq(expectedObjects));
            else
                EXPECT_THAT(getObjectsFunction("I don't exist"), ContainerEq(ObjectsT()));
        }

        template<typename ObjectT, typename IndexObjectT, typename SourceObejctsT, typename SourceIndexObjectsT>
        void testGetObjectByIndexObject(
            boost::function<ObjectT(IndexObjectT)> getObjectsFunction, bool existObject,
            const SourceObejctsT& sourceData, const SourceIndexObjectsT& indexSourceData)
        {
            const char* tmp[] = {"x", "y", "x", "what", "hi"};
            const size_t size = sizeof(tmp) / sizeof(char*);

            ObjectNames names = createObjectNames(tmp, size);

            fillData(names);

            if (existObject)
            {
                for (size_t i = 0; i < size; ++i)
                {
                    EXPECT_EQ(sourceData[i], getObjectsFunction(indexSourceData[i]));
                }
            }
            else
            {
                SceneObjectStub sceneObject;

                EXPECT_EQ(nullptr, mapping->modelObject(&sceneObject));
            }
        }
    };

    // ----------------------------------------------------------------------------------------

    TEST_F(ModelSceneMappingTemplateTest, releaseShouldClearScene)
    {
        const char* tmp[] = {"test", "test", "test", "x", "y"};
        const size_t size = sizeof(tmp) / sizeof(char*);
        ObjectNames names = createObjectNames(tmp, size);
        fillData(names);

        EXPECT_CALL(*scene.get(), destroyGeometry(_)).Times(size);

        mapping->release();
    }

    TEST_F(ModelSceneMappingTemplateTest, destroyShouldClearSceneIfReleaseWasNotCalled)
    {
        const char* tmp[] = {"test", "test", "test", "x", "y"};
        const size_t size = sizeof(tmp) / sizeof(char*);
        ObjectNames names = createObjectNames(tmp, size);
        fillData(names);

        EXPECT_CALL(*scene.get(), destroyGeometry(_)).Times(size);

        mapping.reset();
    }

    TEST_F(ModelSceneMappingTemplateTest, mappingShouldBeEmptyAfterRelease)
    {
        const char* tmp[] = {"test", "test", "test", "x", "y"};
        const size_t size = sizeof(tmp) / sizeof(char*);
        ObjectNames names = createObjectNames(tmp, size);
        fillData(names);

        mapping->release();

        for (ObjectNames::const_iterator it = names.begin(); it != names.end(); ++it)
        {
            EXPECT_EQ(0U, getModelObjects(*it).size());
            EXPECT_EQ(0U, getSceneObjects(*it).size());
        }
    }

    // ----------------------------------------------------------------------------------------

    struct ModelSceneMappingModelObjectTest : public ModelSceneMappingTemplateTest
    {
    };

    TEST_F(ModelSceneMappingModelObjectTest, modelObjectsShouldReturnAllSuitableModelObjects)
    {
        testGetObjectsByName<ModelObjectStub*, TestModelSceneMapping::ModelObjects>(
            getModelObjects, true, modelObjectPtrs);
    }

    TEST_F(ModelSceneMappingModelObjectTest, modelObjectsShouldReturnEmptyListByUnknownName)
    {
        testGetObjectsByName<ModelObjectStub*, TestModelSceneMapping::ModelObjects>(
            getModelObjects, false, modelObjectPtrs);
    }

    TEST_F(ModelSceneMappingModelObjectTest, modelObjectShouldReturnSuitableModelObjectBySceneObject)
    {
        testGetObjectByIndexObject<ModelObjectStub*, SceneObjectStub*>(
            getModelObject, true, modelObjectPtrs, sceneObjectPtrs);
    }

    TEST_F(ModelSceneMappingModelObjectTest, modelObjectShouldReturnNullptrByUnknownSceneObject)
    {
        testGetObjectByIndexObject<ModelObjectStub*, SceneObjectStub*>(
            getModelObject, false, modelObjectPtrs, sceneObjectPtrs);
    }


    // ----------------------------------------------------------------------------------------

    struct ModelSceneMappingSceneObjectTest : public ModelSceneMappingTemplateTest
    {
    };

    TEST_F(ModelSceneMappingSceneObjectTest, sceneObjectsShouldReturnAllSuitableSceneObjects)
    {
        testGetObjectsByName<SceneObjectStub*, TestModelSceneMapping::SceneObjects>(
            getSceneObjects, true, sceneObjectPtrs);
    }

    TEST_F(ModelSceneMappingSceneObjectTest, sceneObjectsShouldReturnEmptyListByUnknownName)
    {
        testGetObjectsByName<SceneObjectStub*, TestModelSceneMapping::SceneObjects>(
            getSceneObjects, false, sceneObjectPtrs);
    }

    TEST_F(ModelSceneMappingSceneObjectTest, sceneObjectShouldReturnSuitableSceneObjectByModelObject)
    {
        testGetObjectByIndexObject<SceneObjectStub*, ModelObjectStub*>(
            getSceneObject, true, sceneObjectPtrs, modelObjectPtrs);
    }

    TEST_F(ModelSceneMappingSceneObjectTest, sceneObjectShouldReturnNullptrByUnknownModelObject)
    {
        testGetObjectByIndexObject<SceneObjectStub*, ModelObjectStub*>(
            getSceneObject, false, sceneObjectPtrs, modelObjectPtrs);
    }
}
}
